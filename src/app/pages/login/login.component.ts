import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[UserService]
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  constructor(private us:UserService, private router:Router, private fb:FormBuilder) {}

  ngOnInit() {
    this.loginForm=this.fb.group({
      'email':[null, [Validators.required, Validators.email]],
      'password':[null, [Validators.required]],
    });
  }

  get f() {return this.loginForm.controls}

  onSubmit() {
    this.us.login(this.loginForm.value).subscribe(data=>{
      localStorage.setItem('access_token', data.access_token);
      this.router.navigate(['tasks']);
    });
  }
}
