import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers:[UserService]
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup
  constructor(private fb:FormBuilder, private us: UserService, private router:Router) { }

  ngOnInit() {
    this.registerForm=this.fb.group({
      'name': [null, Validators.required],
      'email': [null, [Validators.required, Validators.email]],
      'password': [null, [Validators.required, Validators.minLength(6), Validators.maxLength(200)]],
      'password_confirmation':[null, Validators.required],
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.us.registerUser(this.registerForm.value).subscribe(data=>{
      this.router.navigate(['login']);
    });
  }
}
