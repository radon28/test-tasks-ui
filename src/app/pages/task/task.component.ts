import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskService } from 'src/app/shared/services/task.service';
import { Task } from 'src/app/shared/interfaces/task';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  providers:[TaskService]
})
export class TaskComponent implements OnInit {
  addForm:FormGroup;
  editForm:FormGroup;
  closeResult:string;
  tasks:Task[];
  editId:Number;

  constructor(private modalService:NgbModal, private router:Router, private ts:TaskService, private fb:FormBuilder) { }

  ngOnInit(): void {
    this.addForm=this.fb.group({
      'body':[null, [Validators.required, Validators.minLength(3), Validators.minLength(200)]]
    });
    this.editForm=this.fb.group({
      'body':[null, [Validators.required, Validators.minLength(3), Validators.minLength(200)]]
    });
    this.ts.getTasks().subscribe(data=>{
      this.tasks=data.data;
    });
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }

  addTask() {
    this.ts.addTask(this.addForm.value).subscribe(data=>{
      alert("Task Added")
      this.tasks.push(data.data);
      this.modalService.dismissAll();
    });
  }

  getTask(task) {
    this.editId=task.id;
    this.editForm.setValue({
      body:task.body
    });
  }

  editTask() {
    this.ts.editTask(this.editId, this.editForm.value).subscribe(data=>{
      alert("Task Updated"),
      this.modalService.dismissAll();
      let index=this.tasks.findIndex((el)=>{
        return el.id===data.data.id
      });
      this.tasks[index].body=data.data.body;
    });
  }

  deleteTask() {
    this.tasks.splice(this.tasks.findIndex((el)=>{
      return el.id===this.editId;
    }), 1);
    this.ts.deleteTask(this.editId).subscribe(data=>{
      alert('Task deleted');
      this.modalService.dismissAll();
    });
  }
}
