import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable()
export class TaskService {
  api_url=environment.api_url;

  constructor(private _http:HttpClient) { }

  getTask(id):Observable<any>{
    return this._http.get(this.api_url+"get-task/"+id);
  }

  getTasks():Observable<any>{
    return this._http.get(this.api_url+"get-tasks");
  }

  addTask(post_data):Observable<any>{
    return this._http.post(this.api_url+"add-task", post_data);
  }

  editTask(id, post_data):Observable<any>{
    return this._http.put(this.api_url+"update-task/"+id, post_data);
  }

  deleteTask(id):Observable<any>{
    return this._http.delete(this.api_url+"delete-task/"+id);
  }
}
