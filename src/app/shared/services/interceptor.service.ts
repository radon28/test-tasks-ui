import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from "rxjs/internal/operators";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private router:Router) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available
    const currentUser = localStorage.getItem('access_token');
    if (request.url !== 'login' && request.url !== 'register') {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${currentUser}`
          }
        });
    }
    return next.handle(request)
    .pipe(tap(
      (response: HttpEvent<any>) => {
        if (response instanceof HttpResponse) {
          // console.log('response interceptor',response)
        }

        // not the same as "next?" parameter of .subscribe
      },
      (error:HttpErrorResponse) => {
        // console.log("Request Error", error.error.type); // this runs when you get error
        // let type=error.error.type;
        if(error.status == 401) {
          localStorage.removeItem('access_token');
          this.router.navigate(['/login']);
        }
        if(error.status==403) {
          this.router.navigate(['/access-denined'], {state:error.error});
        }
        if(error.status==404) {
          this.router.navigate(['/not-found'], {state:error.error});
        }
        if(error.status==500) {
          this.router.navigate(['/server-error'], {state:error.error});
        }
        // same as "error?" parameter of .subscribe
      },
      () => {
        //console.log("completed successfully"); // this runs when you don't get error
        // same as "complete?" parameter of .subscribe

      }
    ));
  }
}
