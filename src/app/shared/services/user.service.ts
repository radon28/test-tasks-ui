import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable()
export class UserService {
  api_url=environment.api_url;

  constructor(private _http:HttpClient) { }

  registerUser(post_data):Observable<any> {
    return this._http.post(this.api_url+'register', post_data);
  }

  login(post_data):Observable<any>{
    return this._http.post(this.api_url+'login', post_data);
  }
}
